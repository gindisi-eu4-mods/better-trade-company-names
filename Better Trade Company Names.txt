Better Trade Company Names v1.0.4 for Europa Universalis IV v1.37

FEATURES:
Changes trade company and charter names to better match trade nodes.
Changes trade company names to use country adjectives rather than culture names.
For example, a French trade company in the Beijing trade node in vanilla would be called "Francien North China Company" while in the mod it would be called "French Beijing Company"

COMPATIBILITY:
Ironman compatible.
Compatible with all mods.

LINKS:
Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=2882822263
Gitlab: https://gitlab.com/gindisi-eu4-mods/better-trade-company-names
Discord: https://discord.gg/EmYyp4UDmN